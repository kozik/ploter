from argparse import ArgumentParser
import serial
import time


parser = ArgumentParser()
parser.add_argument("-s", "--serial", dest="serial_name", help="select serial port", default="/dev/ttyUSB0")
parser.add_argument("-f", "--file", dest="filename", help="file with g-code")
args = parser.parse_args()

print("Open port {}".format(args.serial_name))
ser = serial.Serial(args.serial_name, 9600)
time.sleep(5)

with open(args.filename) as fp:
    line = fp.readline()
    while line:
        if 'G' not in line:
            line = fp.readline()
            continue
        print(line[:-1])
        ser.write(line.encode())
        res = ser.readline()
        print(res[:-1])
        line = fp.readline()

ser.close()
print("END")
