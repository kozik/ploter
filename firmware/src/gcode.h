#ifndef GCODE_H
#define GCODE_H

typedef struct state {
    int code;
    int val[3];
    int axes[3];
    int all_axes;
    int p_val;
    int tmp;

    void (*fun_base)(int, int, int);
    void (*fun_move0)(int, int, int);
    void (*fun_move1)(int, int, int);
} state;

int parse_number(char c, int *n);
int parse_xyz(state *s, char c);
void gcode_init(state *s);
void gcode_parse(state *s, char c);

#endif /* GCODE_H */
