from argparse import ArgumentParser
from svgpathtools import *
import os
import time


xmin = 0
xmax = 100
ymin = 0
ymax = 80


def parse_point(p, ):
    px = round(p.real)
    py = round(p.imag)

    if px < xmin:
        px = xmin
    elif px > xmax:
        px = xmax

    if py < ymin:
        py = ymin
    elif py > ymax:
        py = ymax

    # Mirrow X against (xmin+xmax)/2
    px = xmax+xmin-px

    return (px, py)


def path_2_gcode(f, points, z):
    for i in range(len(points)):
        f.write("G1 X{} Y{}\n".format(points[i][0], points[i][1]))
        if i == 0:
            f.write("G0 Z{}\n".format(z))
    f.write("G0 Z0\n")
    f.write("\n")


parser = ArgumentParser()
parser.add_argument("-f", "--file", dest="filename", help="svg file")
parser.add_argument("-z", "--z", dest="z", help="z - working position of tool", default=50)
parser.add_argument('-b', "--border", dest="b", help="Add border", action='store_true')
parser.add_argument('-v', "--verbose", dest="v", help="Verbose", action='store_true')
args = parser.parse_args()

z = args.z
v = args.v
b = args.b

filename, extension = os.path.splitext(args.filename)
dst_file = "{}.gcode".format(filename)
if v:
    print("Destination file: {}".format(dst_file))
f = open(dst_file, "w")

paths, attributes = svg2paths(args.filename)

f.write("G28\n")
f.write("\n")

if b:
    points = [(xmin,ymin), (xmin, ymax), (xmax, ymax), (xmax, ymin), (xmin, ymin)]
    path_2_gcode(f, points, z)

for path in paths:
    if v:
        print(path)
    valid_path = 1
    points = []
    for line in path:
        if v:
            print(line)
        if not isinstance(line, Line):
            valid_path = 0
            break
        if len(points) == 0:
            points.append(parse_point(line.start))
        points.append(parse_point(line.end))
    if not valid_path:
        print("Only lines supported!")
        continue
    path_2_gcode(f, points, z)
    if v:
        print(points)
    if v:
        print("----")

f.write("G28\n")
f.close()
