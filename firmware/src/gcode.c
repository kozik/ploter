#include "gcode.h"

int parse_number(char c, int *n)
{
    if (c >= '0' && c <= '9') {
        *n = *n * 10 + c - '0';
        return 0;
    }
    if (c == ' ' || c == '\n')
        return 1;
    return -1;
}

int parse_xyz(state *s, char c)
{
    int ret;

    if (s->tmp == -1) {
        switch (c) {
            case 'X':
                s->tmp = 0;
                s->val[0] = 0;
                break;
            case 'Y':
                s->tmp = 1;
                s->val[1] = 0;
                break;
            case 'Z':
                s->tmp = 2;
                s->val[2] = 0;
                break;
            default:
                return -1;
        }
    } else {
        ret = parse_number(c, &(s->val[s->tmp]));
        if (ret == 1)
            s->tmp = -1;
        else if (ret == -1)
            return -1;
    }
    return 0;
}

void gcode_init(state *s)
{
    s->code = -1;
    for (int i=0; i<3; i++) {
        s->val[i] = -1;
        s->axes[i] = 0;
    }
    s->all_axes = 0;
    s->p_val = 0;
    s->tmp = 0;
}

void gcode_parse(state *s, char c)
{
    int ret;

    if (c == '\r')
        return;

    if (s->code == -1) {
        if (c == 'G') {
            s->code = -2;
        }
    } else if (s->code == -2) {
        ret = parse_number(c, &(s->tmp));
        if (ret == -1) {
            gcode_init(s);
        } else if (ret == 1) {
            s->code = s->tmp;
            if (s->code == 0 || s->code == 1)
                s->tmp = -1;
            else
                s->tmp = 0;
            if (s->code == 28) {
                s->all_axes = 1;
                if (c == '\n')
                    s->fun_base(1,1,1);
            }
        }
    } else if (s->code == 0) {
        parse_xyz(s, c);
        if (c == '\n') {
            s->fun_move0(s->val[0], s->val[1], s->val[2]);
        }
    } else if (s->code == 1) {
        parse_xyz(s, c);
        if (c == '\n') {
            s->fun_move1(s->val[0], s->val[1], s->val[2]);
        }
    } else if (s->code == 28) {
        switch (c) {
            case 'X':
                s->all_axes = 0;
                s->axes[0] = 1;
                break;
            case 'Y':
                s->all_axes = 0;
                s->axes[1] = 1;
                break;
            case 'Z':
                s->all_axes = 0;
                s->axes[2] = 1;
                break;
            case '\n':
                if (s->all_axes)
                    s->fun_base(1,1,1);
                else
                    s->fun_base(s->axes[0], s->axes[1], s->axes[2]);
                break;
        }
    }

    if (c == '\n')
       gcode_init(s); 
}

