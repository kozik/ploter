#include <Stepper.h>
extern "C" {
  #include "src/gcode.h"
}

enum direction {
  X = 0,
  Y = 1,
  Z = 2
};

const int stepsPerRevolution = 256;
const int B[3] = {A2, A1, A0};
const int DIR[3] = {-1, -1, -1};
const int STEPS_PER_UNIT[3] = {32, 32, 5};

Stepper stepperX(stepsPerRevolution, 8, 10, 9, 11);
Stepper stepperY(stepsPerRevolution, 4, 6, 5, 7);
Stepper stepperZ(stepsPerRevolution, 2, 12, 3, 13);
Stepper *steppers[3] = {&stepperX, &stepperY, &stepperZ};

state s;
long pos[3];

void base(int x, int y, int z) {
  int check[3] = {x, y, z};
  for (int i = 2; i >= 0; i--) {
    if (check[i] == 0)
      continue;
    steppers[i]->setSpeed(100);

    while (digitalRead(B[i]) != 0)
      steppers[i]->step(-DIR[i] * 5);

    steppers[i]->setSpeed(10);
    while (digitalRead(B[i]) == 0) 
      steppers[i]->step(DIR[i] * 5);
    steppers[i]->step(DIR[i] * 20);

    while (digitalRead(B[i]) != 0)
      steppers[i]->step(-DIR[i]);
    pos[i] = 0;
    steppers[i]->setSpeed(50);
  }
  Serial.println("OK");
}

void move0(int x, int y, int z)
{
  long dest[3] = {x, y, z};
  for (int i = 0; i < 3; i++) {
    if (dest[i] == -1)
      continue;
    dest[i] *= STEPS_PER_UNIT[i];
    long to_move = dest[i] - pos[i];
    steppers[i]->step(DIR[i] * to_move);
    pos[i] = dest[i];
  }
  Serial.println("OK");
}

void move1(int x, int y, int z)
{
  int N = 100;
  long dest[3] = {x, y, z};
  long last_dest[3] = {0,0,0};

  for (int i = 0; i < 3; i++) {
    if (dest[i] == -1)
      dest[i] = 0;
    else
      dest[i] = dest[i]*STEPS_PER_UNIT[i] - pos[i];
  }

  for (int n = 1; n <= N; n++) {
    for (int i = 0; i < 3; i++) {
      long offset = dest[i];
      offset *= n;
      offset /= N;
      long to_move = offset - last_dest[i];
      last_dest[i] += to_move;
      steppers[i]->step(DIR[i] * to_move);
      pos[i] += to_move;
    }
  }
  
  Serial.println("OK");
}

void setup() {
  for (int i=0; i < 3; i++) {
    pinMode(B[i], INPUT_PULLUP);
    steppers[i]->setSpeed(50);
  }
  Serial.begin(9600);
  gcode_init(&s);
  s.fun_base = base;
  s.fun_move0 = move0;
  s.fun_move1 = move1;
}

void loop() {
  while (Serial.available() == 0);  
  gcode_parse(&s, Serial.read());
}
