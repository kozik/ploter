'use strict';

let port;
let reader;
let inputDone;
let outputDone;
let inputStream;
let outputStream;

const butConnect = document.getElementById('butConnect');
const butG28 = document.getElementById('butG28');
const butG00 = document.getElementById('butG00');
const butG01 = document.getElementById('butG01');
const butSend = document.getElementById('butSend');
const butToGcode = document.getElementById('butToGcode');

const textX = document.getElementById('textX');
const textY = document.getElementById('textY');
const textZ = document.getElementById('textZ');
const textGcode = document.getElementById('textGcode');
const textSvgZ = document.getElementById('textSvgZ');

const fileSvg = document.getElementById('fileSvg');
const chSvgB = document.getElementById('chSvgB');

const divInfo = document.getElementById('info');

document.addEventListener('DOMContentLoaded', () => {
  port = null;
  butConnect.addEventListener('click', clickConnect);
  butG28.addEventListener('click', clickG28);
  butG00.addEventListener('click', clickG00);
  butG01.addEventListener('click', clickG01);
  butSend.addEventListener('click', send_gcode);
  butToGcode.addEventListener('click', convertSvg);
  fileSvg.addEventListener('change', loadSvg);

  if (!('serial' in navigator)) {
    setInfo(
      "Sorry, <b>Web Serial</b> is not supported on this device, make sure you're \
       running Chrome 78 or later and have enabled the \
       <code>#enable-experimental-web-platform-features</code> flag in \
       <code>chrome://flags</code>", "alert-danger");
   }
});

/**
 * @name connect
 * Opens a Web Serial connection to a micro:bit and sets up the input and
 * output stream.
 */
async function connect() {
  // CODELAB: Add code to request & open port here.
  port = await navigator.serial.requestPort();
  await port.open({ baudRate: 9600 });

  // CODELAB: Add code to read the stream here.
  let decoder = new TextDecoderStream();
  inputDone = port.readable.pipeTo(decoder.writable);
  inputStream = decoder.readable
    .pipeThrough(new TransformStream(new LineBreakTransformer()));

  reader = inputStream.getReader();

  // CODELAB: Add code setup the output stream here.
  const encoder = new TextEncoderStream();
  outputDone = encoder.readable.pipeTo(port.writable);
  outputStream = encoder.writable;
}


/**
 * @name disconnect
 * Closes the Web Serial connection.
 */
async function disconnect() {
  // CODELAB: Close the input stream (reader).
  if (reader) {
    await reader.cancel();
    await inputDone.catch(() => {});
    reader = null;
    inputDone = null;
  }
  // CODELAB: Close the output stream.
  if (outputStream) {
    await outputStream.getWriter().close();
    await outputDone;
    outputStream = null;
    outputDone = null;
  }
  // CODELAB: Close the port.
  await port.close();
  port = null;
}


/**
 * @name clickConnect
 * Click handler for the connect/disconnect button.
 */
async function clickConnect() {
  // CODELAB: Add disconnect code here.
  if (port) {
    await disconnect();
    toggleUIConnected(false);
    return;
  }
  // CODELAB: Add connect code here.
  await connect();
  toggleUIConnected(true);
}

async function clickG28() {
  if (!port) {
    setInfo("Disconnected", "alert-warning");
    return;
  }
  await send_wait("G28");
}

async function clickG00() {
  if (!port) {
    setInfo("Disconnected", "alert-warning");
    return;
  }
  await send_wait("G00 " + getXYZ());
}

async function clickG01() {
  if (!port) {
    setInfo("Disconnected", "alert-warning");
    return;
  }
  await send_wait("G01 " + getXYZ());
}

function getXYZ() {
  return "X" + textX.value + " Y" + textY.value +  " Z" + textZ.value;
}

async function send_gcode() {
  if (!port) {
    setInfo("Disconnected", "alert-warning");
    return;
  }
  var data = textGcode.value.split('\n');
  for (var i = 0; i < data.length; i++) {
    await send_wait(data[i] + "\n");
  }
}

/**
 * @name writeToStream
 * Gets a writer from the output stream and send the lines to the micro:bit.
 * @param  {...string} lines lines to send to the micro:bit
 */
function writeToStream(...lines) {
  // CODELAB: Write to output stream
  const writer = outputStream.getWriter();
  lines.forEach((line) => {
    console.log('[SEND]', line);
    writer.write(line + '\n');
  });
  writer.releaseLock();
}

function toggleUIConnected(connected) {
  let lbl = 'Connect';
  if (connected) {
    lbl = 'Disconnect';
    setInfo("Connected", "alert-success");
  } else {
    setInfo("Not connected", "alert-dark");
  }
  butConnect.textContent = lbl;
}

/**
 * @name LineBreakTransformer
 * TransformStream to parse the stream into lines.
 */
class LineBreakTransformer {
  constructor() {
    // A container for holding stream data until a new line.
    this.container = '';
  }

  transform(chunk, controller) {
    // CODELAB: Handle incoming chunk
    this.container += chunk;
    const lines = this.container.split('\n');
    this.container = lines.pop();
    lines.forEach(line => controller.enqueue(line));
  }

  flush(controller) {
    // CODELAB: Flush the stream.
    controller.enqueue(this.container);
  }
}

async function send_wait(cmd) {
  var isOk = 0;
  setInfo("Sending: " + cmd, "alert-info");
  writeToStream(cmd);
  while (isOk == 0) {
    const { value, done } = await reader.read();
    if (value) {
      if (value.includes('OK')) {
        isOk = 1;
        setInfo("Done: " + cmd, "alert-success");
      }
    }
    if (done) {
      console.log('[readLoop] DONE', done);
      reader.releaseLock();
    }
  }
}

function setInfo(text, color) {
  divInfo.innerHTML = text;
  divInfo.className = "alert " + color;
}

async function loadSvg() {
  if (fileSvg.files.length === 0) {
    return;
  }
  if (fileSvg.files.length > 1) {
    return;
  }

  const img = fileSvg.files[0];
  var url = URL.createObjectURL(img);

  var xhr = new XMLHttpRequest();
  xhr.open("GET", url, false);
  xhr.overrideMimeType("image/svg+xml");
  xhr.onload = function(e) {
    document.getElementById("svgContainer").innerHTML = "";
    document.getElementById("svgContainer")
      .appendChild(xhr.responseXML.documentElement);
  }
  xhr.send("");

}

async function convertSvg() {
  var text = "G28\r\n";
  const xmin = 0;
  const xmax = 100;
  const ymin = 0;
  const ymax = 80; 
  const z = textSvgZ.value;

  if (fileSvg.files.length === 0) {
    setInfo("No .svg file selected", "alert-warning");
    return;
  }
  if (fileSvg.files.length > 1) {
    setInfo("Select only one file.", "alert-warning");
    return;
  }

  const img = fileSvg.files[0];
  var url = URL.createObjectURL(img);

  var xhr = new XMLHttpRequest();
  xhr.open("GET", url, false);
  xhr.overrideMimeType("image/svg+xml");
  xhr.onload = function(e) {
    document.getElementById("svgContainer").innerHTML = "";
    document.getElementById("svgContainer")
      .appendChild(xhr.responseXML.documentElement);
  }
  xhr.send("");

  if (chSvgB.checked == true) {
    const border = [[xmin,ymin], [xmin, ymax], [xmax, ymax], [xmax, ymin], [xmin, ymin]];
    text += path2gcode(border, z);
  }

  var paths = document.getElementById("svgContainer").getElementsByTagName('path');

  for ( var p=0; p<paths.length; p++) {
    var path = paths[p];
    var len = path.getTotalLength();
    var point = path.getPointAtLength(0);

    var l = Math.ceil(len/5);

    for ( var i=0; i<=l; i++ ) {
      var point = path.getPointAtLength( i/l * len);
      var x = Math.round(point.x);
      if ( x > xmax) x = xmax;
      if ( x < xmin) x = xmin;
      // Mirrow X against (xmin+xmax)/2
      x = xmax + xmin - x;
      var y = Math.round(point.y);
      if ( y > ymax) y = ymax;
      if ( y < ymin) y = ymin;
      text += "G1 X" + x + " Y" + y +"\r\n";
      if (i == 0)
        text += "G0 Z" + z + "\r\n";
    }
    text += "G0 Z0\r\n";
  }

  text += "G28";
  textGcode.value = text; 
  setInfo("G-CODE generated.", "alert-success");
}

function path2gcode(points, z) {
    var text ="";

    for (var i = 0; i < points.length; i++) {
      text += "G1 X" + points[i][0] +" Y" + points[i][1] + "\r\n";
      if (i == 0)
        text += "G0 Z" + z + "\r\n";
    }
    text += "G0 Z0\r\n";
    return text;
}

