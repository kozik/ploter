#include "gtest/gtest.h"
extern "C" {
    #include "gcode.h"
}

static int base_x, base_y, base_z;
static int move0_x, move0_y, move0_z;
static int move1_x, move1_y, move1_z;

static void fun_base(int x, int y, int z)
{
    base_x = x;
    base_y = y;
    base_z = z;
}

static void fun_move0(int x, int y, int z)
{
    move0_x = x;
    move0_y = y;
    move0_z = z;
}

static void fun_move1(int x, int y, int z)
{
    move1_x = x;
    move1_y = y;
    move1_z = z;
}

static void fun_reset()
{
    base_x = base_y = base_z = 0;
    move0_x = move0_y = move0_z = 0;
    move1_x = move1_y = move1_z = 0;
}

static void fun_init(state *s)
{
    s->fun_base = fun_base;
    s->fun_move0 = fun_move0;
    s->fun_move1 = fun_move1;
}

TEST(gcode, parse_number)
{
    int res, n;
    n = 0;
    res = parse_number('1', &n);
    ASSERT_EQ(1, n);
    ASSERT_EQ(0, res);
    res = parse_number('2', &n);
    ASSERT_EQ(12, n);
    ASSERT_EQ(0, res);
    res = parse_number(' ', &n);
    ASSERT_EQ(12, n);
    ASSERT_EQ(1, res);
}

static void send_str(state *s, const char *str)
{
    for (int i = 0; str[i] != '\0'; i++)
        gcode_parse(s, str[i]); 
}

void check_init(state *s)
{
    ASSERT_EQ(-1, s->code);
    for (int i=0; i<3; i++) {
        ASSERT_EQ(-1, s->val[i]);
        ASSERT_EQ(0, s->axes[i]);
    }
    ASSERT_EQ(0, s->all_axes);
    ASSERT_EQ(0, s->p_val);
    ASSERT_EQ(0, s->tmp);
}

TEST(gcode, state_reset_0)
{
    state s;

    fun_init(&s);

    send_str(&s, "\n");
    check_init(&s);
}

TEST(gcode, state_reset_1)
{
    state s;

    fun_init(&s);

    send_str(&s, "G28\nG0 X12 Y45 Z23\r\nG9 X23 90\n");
    check_init(&s);
}

TEST(gcode, parse_code_number)
{
    state s;

    fun_init(&s);

    gcode_init(&s);
    send_str(&s, "G28 ");
    ASSERT_EQ(28, s.code);
    ASSERT_EQ(0, s.tmp);
    ASSERT_EQ(1, s.all_axes);

    gcode_init(&s);
    send_str(&s, "G28 ");
    ASSERT_EQ(28, s.code);
    ASSERT_EQ(0, s.tmp);
    ASSERT_EQ(1, s.all_axes);
}

static void send_strXYZ(state *s, const char *str)
{
    for (int i = 0; str[i] != '\0'; i++)
        parse_xyz(s, str[i]); 
}

TEST(gcode, parse_XYZ)
{
    state s;

    fun_init(&s);
    gcode_init(&s);
    s.tmp = -1;

    send_strXYZ(&s, "X100");
    ASSERT_EQ(100, s.val[0]);
    ASSERT_EQ(-1, s.val[1]);
    ASSERT_EQ(-1, s.val[2]);

    gcode_init(&s);
    s.tmp = -1;
    send_strXYZ(&s, "X100 Y30 Z15");
    ASSERT_EQ(100, s.val[0]);
    ASSERT_EQ(30, s.val[1]);
    ASSERT_EQ(15, s.val[2]);

    gcode_init(&s);
    s.tmp = -1;
    send_strXYZ(&s, "Z100 X30 Y15");
    ASSERT_EQ(30, s.val[0]);
    ASSERT_EQ(15, s.val[1]);
    ASSERT_EQ(100, s.val[2]);
}

TEST(gcode, G0_0)
{
    state s;

    fun_init(&s);
    gcode_init(&s);
    send_str(&s, "G0 X123");
    ASSERT_EQ(0, s.code);
    ASSERT_EQ(0, s.tmp);
    ASSERT_EQ(123, s.val[0]);
    ASSERT_EQ(-1, s.val[1]);
    ASSERT_EQ(-1, s.val[2]);
}

TEST(gcode, G0_1)
{
    state s;

    fun_init(&s);
    gcode_init(&s);
    send_str(&s, "G0 X450 Y23 Z10");
    ASSERT_EQ(0, s.code);
    ASSERT_EQ(450, s.val[0]);
    ASSERT_EQ(23, s.val[1]);
    ASSERT_EQ(10, s.val[2]);
}

TEST(gcode, G0_2)
{
    state s;

    fun_init(&s);
    gcode_init(&s);
    send_str(&s, "G0 Z30 X10");
    ASSERT_EQ(0, s.code);
    ASSERT_EQ(10, s.val[0]);
    ASSERT_EQ(-1, s.val[1]);
    ASSERT_EQ(30, s.val[2]);
}

TEST(gcode, G1_0)
{
    state s;

    fun_init(&s);
    gcode_init(&s);
    send_str(&s, "G1 Y2357");
    ASSERT_EQ(1, s.code);
    ASSERT_EQ(-1, s.val[0]);
    ASSERT_EQ(2357, s.val[1]);
    ASSERT_EQ(-1, s.val[2]);
}

TEST(gcode, G1_1)
{
    state s;

    fun_init(&s);
    gcode_init(&s);
    send_str(&s, "G1 X450 Y23 Z10");
    ASSERT_EQ(1, s.code);
    ASSERT_EQ(450, s.val[0]);
    ASSERT_EQ(23, s.val[1]);
    ASSERT_EQ(10, s.val[2]);
}

TEST(gcode, G1_2)
{
    state s;

    fun_init(&s);
    gcode_init(&s);
    send_str(&s, "G1 Z30 X10");
    ASSERT_EQ(1, s.code);
    ASSERT_EQ(10, s.val[0]);
    ASSERT_EQ(-1, s.val[1]);
    ASSERT_EQ(30, s.val[2]);
}

TEST(gcode, G28_0)
{
    state s;

    fun_init(&s);
    gcode_init(&s);
    send_str(&s, "G28 ");
    ASSERT_EQ(28, s.code);
    ASSERT_EQ(1, s.all_axes);
}

TEST(gcode, G28_1)
{
    state s;

    fun_init(&s);
    gcode_init(&s);
    send_str(&s, "G28 X");
    ASSERT_EQ(28, s.code);
    ASSERT_EQ(0, s.all_axes);
    ASSERT_EQ(1, s.axes[0]);
    ASSERT_EQ(0, s.axes[1]);
    ASSERT_EQ(0, s.axes[2]);
}

TEST(gcode, G28_2)
{
    state s;

    fun_init(&s);
    gcode_init(&s);
    send_str(&s, "G28 Y X");
    ASSERT_EQ(28, s.code);
    ASSERT_EQ(0, s.all_axes);
    ASSERT_EQ(1, s.axes[0]);
    ASSERT_EQ(1, s.axes[1]);
    ASSERT_EQ(0, s.axes[2]);
}

TEST(gcode, G28_3)
{
    state s;

    fun_init(&s);
    gcode_init(&s);
    send_str(&s, "G28 X Y Z");
    ASSERT_EQ(28, s.code);
    ASSERT_EQ(0, s.all_axes);
    ASSERT_EQ(1, s.axes[0]);
    ASSERT_EQ(1, s.axes[1]);
    ASSERT_EQ(1, s.axes[2]);
}

TEST(gcode, f_G28_0)
{
    state s;

    fun_init(&s);
    gcode_init(&s);
    fun_reset();
    send_str(&s, "G28\n");

    ASSERT_EQ(1, base_x);
    ASSERT_EQ(1, base_y);
    ASSERT_EQ(1, base_z);
}

TEST(gcode, f_G28_1)
{
    state s;

    fun_init(&s);
    gcode_init(&s);
    fun_reset();
    send_str(&s, "G28 Y\n");

    ASSERT_EQ(0, base_x);
    ASSERT_EQ(1, base_y);
    ASSERT_EQ(0, base_z);
}

TEST(gcode, f_G28_2)
{
    state s;

    fun_init(&s);
    gcode_init(&s);
    fun_reset();
    send_str(&s, "G28 X Y Z\n");

    ASSERT_EQ(1, base_x);
    ASSERT_EQ(1, base_y);
    ASSERT_EQ(1, base_z);
}

TEST(gcode, f_G0_0)
{
    state s;

    fun_init(&s);
    gcode_init(&s);
    fun_reset();
    send_str(&s, "G0 X100 Y345 Z21\n");

    ASSERT_EQ(100, move0_x);
    ASSERT_EQ(345, move0_y);
    ASSERT_EQ(21, move0_z);
}

TEST(gcode, f_G0_1)
{
    state s;

    fun_init(&s);
    gcode_init(&s);
    fun_reset();
    send_str(&s, "G0 Z100 Y345\n");

    ASSERT_EQ(-1, move0_x);
    ASSERT_EQ(345, move0_y);
    ASSERT_EQ(100, move0_z);
}

TEST(gcode, f_G1_0)
{
    state s;

    fun_init(&s);
    gcode_init(&s);
    fun_reset();
    send_str(&s, "G1 Z100 X10 Y345\n");

    ASSERT_EQ(10, move1_x);
    ASSERT_EQ(345, move1_y);
    ASSERT_EQ(100, move1_z);
}

TEST(gcode, f_G1_1)
{
    state s;

    fun_init(&s);
    gcode_init(&s);
    fun_reset();
    send_str(&s, "G1 Z100 X10\n");

    ASSERT_EQ(10, move1_x);
    ASSERT_EQ(-1, move1_y);
    ASSERT_EQ(100, move1_z);
}

