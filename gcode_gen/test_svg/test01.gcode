G28

G1 X0 Y0
G0 Z50
G1 X0 Y80
G1 X100 Y80
G1 X100 Y0
G1 X0 Y0
G0 Z0

G1 X40 Y63
G0 Z50
G1 X28 Y61
G1 X22 Y71
G1 X19 Y60
G1 X6 Y60
G1 X15 Y51
G1 X8 Y40
G1 X20 Y43
G1 X27 Y32
G1 X30 Y44
G1 X43 Y44
G1 X34 Y53
G1 X40 Y63
G0 Z0

G1 X90 Y34
G0 Z50
G1 X73 Y42
G1 X58 Y31
G1 X60 Y13
G1 X77 Y5
G1 X92 Y16
G1 X90 Y34
G0 Z0

G1 X54 Y55
G0 Z50
G1 X65 Y74
G1 X92 Y53
G1 X54 Y55
G0 Z0

G1 X15 Y8
G0 Z50
G1 X37 Y8
G1 X37 Y25
G1 X15 Y25
G1 X15 Y8
G0 Z0

G28
